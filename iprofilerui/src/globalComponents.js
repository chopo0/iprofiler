import DropDown from './components/Dropdown.vue'
import PapaParse from '../node_modules/papaparse'

/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalComponents = {
  install (Vue) {
    Vue.component('drop-down', DropDown)
    Vue.use(PapaParse)
  }
}

export default GlobalComponents
