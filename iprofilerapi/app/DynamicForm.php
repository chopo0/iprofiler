<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DynamicForm extends Model
{
    protected $table = 'dy_forms';

    public function segmentDetails()
    {
        return $this->hasMany('App\SegmentDetail');
    }

    public function filledForms()
    {
        return $this->hasMany('App\FilledForm', 'f_id', 'key_name');
    }
}
