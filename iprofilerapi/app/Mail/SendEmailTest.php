<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Log;
use Mail;

class SendEmailTest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $mailSubject = '';
    public $mailFrom = '';
    public $mailContent = '';

    public function __construct($email)
    {
        //
        $this->mailSubject = $email['mailSubject'];
        $this->mailFrom = $email['mailFrom'];
        $this->mailContent = $email['mailContent'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('I-ProfilerPro@corporation.com', $this->mailFrom)
            ->subject($this->mailSubject)
            ->view('emails.send')
            ->with(['mailContent'=>$this->mailContent]);
    }
}
