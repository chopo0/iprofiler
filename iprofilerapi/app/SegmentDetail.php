<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SegmentDetail extends Model
{
    protected $touches = ['segment'];

    public function segment()
    {
        return $this->belongsTo('App\Segment');
    }

    public function form()
    {
        return $this->belongsTo('App\DynamicForm');
    }
}
