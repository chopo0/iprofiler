<?php

namespace App\Http\Controllers;

use App\DynamicForm;
use App\FilledForm;
use App\Segment;
use App\SegmentDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SegmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Segment::with(['creator', 'rules'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'brand_of_focus' => 'required',
            'created_by' => 'required|numeric',
            'rules' => 'required|array',
            'rules.*.field' => 'required',
            'rules.*.value' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->getMessageBag()->toArray()], 200);
        }

        $now = Carbon::now();
        $segment = new Segment;

        $segment->name = $request->name;
        $segment->description = $request->description;
        $segment->brand_of_focus = $request->brand_of_focus;
        $segment->created_by = $request->created_by;
        $segment->created_at = $now;

        $segment->save();

        if (!$segment) {
            return response()->json(['errors' => ['db_error' => 'Failed to insert into database']], 200);
        }

        foreach ($request->rules as $rule) {
            $segmentRule = new SegmentDetail;
            $segmentRule->field = $rule['field'];
            $segmentRule->value = $rule['value'];
            $segmentRule->created_at = $now;

            $segment->rules()->save($segmentRule);
        }

        return response()->json($segment, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $success = false;
        $segment = Segment::find($id);
        if ($segment) {
            $segment->rules()->delete();
            $segment->delete();
            $success = true;
        }

        return response()->json(['success' => $success]);

    }

    public function getFields()
    {
        $uniqueFields = [];
        $dynamicForms = DynamicForm::has('filledForms')->get();
        foreach ($dynamicForms as $dynamicForm) {
            $fieldsStr = $dynamicForm->fields;
            $fieldsArr = explode(',', $fieldsStr);
            foreach ($fieldsArr as $field) {
                if (!empty($field) && !in_array(strtolower($field), $uniqueFields)) {
                    $uniqueFields[] = strtolower($field);
                }
            }
        }

        return response()->json($uniqueFields);
    }
}
