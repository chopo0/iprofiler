<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    public function rules()
    {
        return $this->hasMany('App\SegmentDetail');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
}
